# NULL-Terminated #

An opportunity to upskill together and share knowledge. A space for learning while making it fun.

## What to do ##

1. Join the #null-terminated slack channel
2. Find the bit bucket repo
3. Clone the Monthly_Challenges repo ( if you haven't already )
4. Switch to the current challenge’s branch `/challenge_name/main`
5. Then create a new branch using the same naming format `/challenge_name/your_name` - Or team name if working together in a group
6. Commit and push regularly, it doesn't need to be finished, it can be a work in progress. This helps if people get stuck and aren’t sure what to do, or can aid in someone jumping into your branch to help you.
7. Challenges are completed and issued at the beginning of the month
8. The Previous challenge is reviewed over the following Morning Teas

## The Hard Rules ##

1. Must be started within the allocated time, so no code started prior / you already had. (default: 1 Month)
    This doesn't stop you from sharing code you already had in the channel, but it won't be considered part of the challenge

### Who do I talk to? ###

1. #null-terminated slack channel
2. Alex
3. Akshara
4. Keegan
